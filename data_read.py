#!/usr/bin/python3
'''
Communicate with air quality sensor.
List serial ports with
python -m serial.tools.list_ports
TODO: export data, plot data

LICENSE: GPL v3+. See LICENSE.txt for full terms.
'''
import serial
import struct
import datetime as dt

#print('Importing plotter')
import plotter
#print('Done')
import writer_average as wr

import logging

logging.basicConfig(filename="/home/pi/Documents/particle_sensor/sample.log",
                    filemode='w', level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

DEBUG = 0
CMD_MODE = 2
CMD_QUERY_DATA = 4
CMD_DEVICE_ID = 5
CMD_SLEEP = 6
CMD_FIRMWARE = 7
CMD_WORKING_PERIOD = 8
MODE_ACTIVE = 0
MODE_QUERY = 1

ser = serial.Serial()
ser.port = "/dev/ttyUSB0"
ser.baudrate = 9600

ser.open()
ser.flushInput()

byte, data = 0, ""


def dump(d, prefix=''):
    print(prefix + ' '.join(x.encode('hex') for x in d))


def construct_command(cmd, data=[]):
    assert len(data) <= 12
    data += [0, ]*(12-len(data))
    checksum = (sum(data)+cmd-2) % 256
    ret = b"\xaa\xb4" + chr(cmd).encode('utf8')
    ret += b''.join(chr(x).encode('utf8') for x in data)
    ret += b"\xff\xff" + chr(checksum).encode('utf8') + b"\xab"

    return ret


def process_data(d):
    r = struct.unpack('<HHxxBB', d[2:])
    pm25 = r[0]/10.0
    pm10 = r[1]/10.0
    checksum = sum(v for v in d[2:8]) % 256

    if checksum == r[2] and r[3] == 0xab:
      #print("PM2.5: {}\tPM10: {}".format(pm25, pm10))
        return pm25, pm10
    else:
        return -1, -1


def process_version(d):
    #r = struct.unpack('<BBBHBB', d[3:])
    struct.unpack('<BBBHBB', d[3:])
    #checksum = sum(v for v in d[2:8]) % 256
    # convert print statement to logging.
    # conditional loggin?
#    logging.debug()
    #print("CRC={}".format( "OK" if (checksum==r[4] and r[5]==0xab) else
#    "NOK"))
    # Whole function is useless without writing to log or printing


def read_response():
    byte = 0
    while byte != b"\xaa":
        byte = ser.read(size=1)

    d = ser.read(size=9)

#	if DEBUG:
#		dump(d, '< ')
    return byte + d


def cmd_set_mode(mode=MODE_QUERY):
    to_write = construct_command(CMD_MODE, [0x1, mode])
    ser.write(to_write)
    read_response()


def cmd_query_data():
    to_write = construct_command(CMD_QUERY_DATA)
    ser.write(to_write)
    d = read_response()
    if hex(d[1]) == '0xc0':
        pm25, pm10 = process_data(d)
    return pm25, pm10


def cmd_set_sleep(sleep=1):
    mode = 0 if sleep else 1
    ser.write(construct_command(CMD_SLEEP, [0x1, mode]))
    read_response()


def cmd_set_working_period(period):
    ser.write(construct_command(CMD_WORKING_PERIOD, [0x1, period]))
    read_response()


def cmd_firmware_ver():
    ser.write(construct_command(CMD_FIRMWARE))
    d = read_response()
    process_version(d)


if __name__ == "__main__":
    #print('Setting mode')
    logging.debug('Starting ...')
    cmd_set_mode(1)
    #print('Firmware version')
    cmd_firmware_ver()
    logging.debug('Successfully set mode and firmware version.')

#    while True:
#    print('Querying data')
    pm25, pm10 = cmd_query_data()
    logging.info(pm25)
    logging.info(pm10)
    now = dt.datetime.now()
    now = now.strftime("%Y-%m-%d %H:%M:%S")
    print('Writing new data ...')
    logging.debug('About to write to csv ...')
    wr.write_to_csv(pm25, pm10, now)
    logging.debug('Wrote to csv.')
    print('Trimming ...')
    logging.debug('About to trim ...')
    try:
        tf_av = wr.trimmer()
    except Exception as e:
        logging.exception("Failed to call trimmer: %s", repr(e))
        raise e
    logging.debug('Trimmed')
    print('Plotting ...')
    logging.debug('Trying to plot ...')
    plotter.plotter('readings_csv.csv', 'plot.svg', tf_av, True)
    plotter.plotter('readings_overflow.csv', 'overflow.svg', tf_av, False)
    logging.debug('Plotted.')
    print('Finished. Waiting ...')
#        time.sleep(10)

    cmd_set_mode(0)
    cmd_set_sleep()
    logging.debug('ending')
