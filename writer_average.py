import logging
import csv
import datetime as dt


def write_to_csv(pm25, pm10, now):
    data_to_write = (pm25, pm10, now)
    with open('/home/pi/Documents/particle_sensor/readings_csv.csv',
		'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(data_to_write)


def half_hour_average():
    # Pandas not imported
    ten_sec = pd.read_csv('readings_csv.csv', header=None, names=['pm25',
				'pm10', 'dt'])
    ten_sec['dt'] = pd.to_datetime(ten_sec['dt'])
 
    half_hour = pd.read_csv('half_hour.csv', header=None, names=['pm25',
				'pm10', 'dt'])
    half_hour['dt'] = pd.to_datetime(half_hour['dt'])

    now = ten_sec['dt'][len(ten_sec)-1]
    then = ten_sec['dt'][0]

    latest_hh = half_hour['dt'][len(half_hour)-1]
    #print(latest_hh)
    drop_index = 0
    for idx, ten in enumerate(ten_sec['dt']):
        if ten < latest_hh:
            drop_index = idx
    ten_sec.drop(ten_sec.index[[list(range(drop_index))]], inplace=True)

    #print(len(ten_sec))
    #print(ten_sec)
    #for idx, ten in enumerate(ten_sec.iterrows()):
        #print(idx)
        #print(idx%30)
        #if not (idx % 30):
        #    print('now')
        #else:
        #    print('not now')


def trimmer():
    log = logging.getLogger("trimmer")
    log.info("in trimmer...")
    with open('/home/pi/Documents/particle_sensor/readings_csv.csv',
   		'r') as readings:
        reader = csv.reader(readings, delimiter=',')
        ten_sec = [row for row in reader]
    log.info("read csv")
    for row in ten_sec:
        row[0] = float(row[0])
        row[1] = float(row[1])
        row[2] = dt.datetime.strptime(row[2], "%Y-%m-%d %H:%M:%S")
    log.info("done some stuff")

    now = ten_sec[len(ten_sec)-1][2]
    then = now - dt.timedelta(hours=24)
    overflow = []
    for idx, ten in enumerate(ten_sec):
        if ten[2] < then:
            overflow.append(ten_sec[idx])
            del ten_sec[idx]

    tf_av = twenty_four_average(ten_sec)
    log.info("averaged")

    with open('/home/pi/Documents/particle_sensor/readings_csv.csv',
    		'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerows(ten_sec)

    log.info("written to readings_csv.csv")
    with open('/home/pi/Documents/particle_sensor/readings_overflow.csv',
   		 'a') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerows(overflow)
    log.info("written to readings_overflow.csv")

    return tf_av


def twenty_four_average(data):
#    print(data)
    # transpose data: 0=2.5, 1=10, 2=dt
    data = list(map(list, zip(*data)))
    pm25_av = round(sum(data[0]) / len(data[0]))
    pm10_av = round(sum(data[1]) / len(data[1]))

    if (0 <= pm25_av < 12) and (0 <= pm10_av < 17):
        text = '1 - Low'
        colour = 'palegreen'
    elif (12 <= pm25_av < 24) and (17 <= pm10_av < 34):
        text = '2 - Low'
        colour = 'lime'
    elif (24 <= pm25_av < 36) and (34 <= pm10_av < 51):
        text = '3 - Low'
        colour = 'green'
    elif (36 <= pm25_av < 42) and (51 <= pm10_av < 59):
        text = '4 - Moderate'
        colour = 'yellow'
    elif (42 <= pm25_av < 48) and (59 <= pm10_av < 67):
        text = '5 - Moderate'
        colour = 'gold'
    elif (48 <= pm25_av < 54) and (67 <= pm10_av < 76):
        text = '6 - Moderate'
        colour = 'orange'
    elif (54 <= pm25_av < 59) and (76 <= pm10_av < 84):
        text = '7 - High'
        colour = 'tomato'
    elif (59 <= pm25_av < 65) and (84 <= pm10_av < 92):
        text = '8 - High'
        colour = 'red'
    elif (65 <= pm25_av < 71) and (92 <= pm10_av < 101):
        text = '9 - High'
        colour = 'darkred'
    elif (71 <= pm25_av) and (101 <= pm10_av):
        text = '10 - Very high'
        colour = 'purple'
    else:
        #print("don't match?")
        colour = 'w'
        text = 'Unclassified'

    return (pm25_av, pm10_av, colour, text)
