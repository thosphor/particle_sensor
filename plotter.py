import matplotlib
matplotlib.use('SVG')
from matplotlib.backends.backend_svg import FigureCanvas
import matplotlib.pyplot as plt
import matplotlib.dates as md
from matplotlib.figure import Figure
import csv
import datetime as dt
import gc


def plotter(file_name, plot_name, tf_av, flag):

    colour = tf_av[2]
    fig_text = tf_av[3]

    with open('/home/pi/Documents/particle_sensor/' + file_name,
              'r') as readings:
        reader = csv.reader(readings, delimiter=',')
        data = [row for row in reader]
    for row in data:
        row[0] = float(row[0])
        row[1] = float(row[1])
        row[2] = dt.datetime.strptime(row[2], "%Y-%m-%d %H:%M:%S")

    data = list(map(list, zip(*data)))
    dates = md.date2num(data[2])

    fig = Figure()
    canvas = FigureCanvas(fig)
    ax = fig.add_subplot(111)
    ax.plot(dates, data[0], label='2.5')
    ax.plot(dates, data[1], label='10')

    xfmt = md.DateFormatter('%H:%M')
    ax.xaxis.set_major_formatter(xfmt)
    ax.legend()
    if flag:
        ax.text(0.1, 0.92, fig_text, color='k', bbox=dict(facecolor=colour,
                edgecolor=colour, boxstyle='round,pad=0.7'),
                transform=ax.transAxes, ha='center', va='center')

    print('saving ...')
    fig.savefig('/home/pi/Documents/particle_sensor/' + plot_name,
                transparent=True)
    plt.close('all')
    del canvas, fig
    gc.collect()
